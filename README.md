# Oz Liveness Sample Web

A sample for Oz Liveness Web SDK: [OzLivenessWebSDKSample.html](OzLivenessWebSDKSample.html). 
To make it work, replace `<web-adapter-url>` with the Web Adapter URL you've received from us.

You can find more information here: [Oz Liveness Web SDK](https://doc.ozforensics.com/oz-knowledge/guides/developer-guide/sdk/oz-liveness-websdk).